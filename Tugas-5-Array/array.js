//Soal 1
console.log("------Soal 1------")
function range(startNum, finishNum){
    var rangeArray = [];

    if (startNum > finishNum){
        rangeLength = startNum - finishNum + 1; 
        for (var i = 0; i < rangeLength; i++){
            rangeArray.push(startNum - i)
        }
    } else if (startNum < finishNum){
        rangeLength = finishNum - startNum + 1;
        for (var i=0; i < rangeLength; i++){
            rangeArray.push(startNum + i)
        }
    } else if (!startNum || !finishNum){
        return -1
    } 
    return rangeArray
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1

console.log(" ")

//Soal 2 
console.log("------Soal 2------")

function rangeWithStep(startNum, finishNum, step){
    var rangeArrayWithStep = [];

    if (startNum > finishNum){
        currentNum = startNum; 
        for (var i = 0; currentNum >= finishNum; i++){
            rangeArrayWithStep.push(currentNum)
            currentNum -= step
        }
    } else if (startNum < finishNum){
        currentNum = startNum;
        for (var i=0; currentNum <= finishNum; i++){
            rangeArrayWithStep.push(currentNum)
            currentNum += step
        }
    } else if (!startNum || !finishNum || !step){
        return -1
    } 
    return rangeArrayWithStep
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log(" ")

//Soal 3
console.log("------Soal 3------")
function sum(awalDeret, akhirDeret, step){
    var rangeArrayWithStep = [];
    var total = 0;
    if (!awalDeret && !akhirDeret && !step){ //cek ketiga parameter ada atau tidak
        return 0 //jika tidak, maka akan mengembalikan nilai 0
    } 
    if (!akhirDeret && !step) { //cek parameter akhir deret dan step ada atau tidak
        return 1 //jika tidak, maka akan mengembalikan nilai 1
    }
    if (awalDeret && akhirDeret){ // cek parameter awal deret dan akhir deret tersedia
        if (!step){ //cek apaka parameter step tersedia atau tidak
            step = 1 //jika tidak, maka nilai step = 1
        }
        if (awalDeret > akhirDeret){ 
            currentNum = awalDeret; 
            for (var i = 0; currentNum >= akhirDeret; i++){
                total += currentNum //nilai currentNum akan ditampung di variabel total
                currentNum -= step
                
            }
        } else if (awalDeret < akhirDeret){
            currentNum = awalDeret;
            for (var i=0; currentNum <= akhirDeret; i++){
                total += currentNum //nilai currentNum akan ditampung di variabel total
                currentNum += step
            }
        }
    }
    return total
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0


console.log(" ")

//Soal 4
console.log("------Soal 4------")
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

function dataHandling(input){
    var data ="";
    for(var i = 0; i < input.length; i++) {
        var dataSementara = input[i];
        for(var j = 0; j <= dataSementara.length; j++) {
            if(j==0){
                data += "Nomor id: "+ dataSementara[j] 
            }else if(j==1){
                data += "\nNama Lengkap: " + dataSementara[j]
            }else if(j==2){
                data += "\nTTL: " + dataSementara[j]
            }else if(j==3){
                data += " " + dataSementara[j] 
            }else if(j==4){
                data += "\nHobi: "+ dataSementara[j] + "\n" + "\n";
            }
        }
    }
    return data
}
console.log(dataHandling(input))

console.log(" ")

//Soal 5 
console.log("------Soal 5------")

function balikKata(str){
    var string =""
    for ( i = str.length -1; i >= 0; i--){
        string += str[i];
    }
    return string
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


console.log(" ")

//Soal 6
console.log("------Soal 6------")

var inputData = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]

function dataHandling2(inputData){
    var inputSementara = [] ;
    var dateSplit = 0;

    inputData.splice(1,1,"Roman Alamsyah Elshawary")
    inputData.splice(2,1,"Provinsi Bandar Lampung")
    inputData.splice(4,0,"Pria")
    inputData.splice(5,1,"SMA Internasional Metro")
    console.log(inputData)

    inputSementara += inputData[3]
    var date = inputSementara.split("/")
    var dateJoin = inputSementara.split("/")

    dateSplit = date[1]; //Menampilkan output "Mei"
    switch (dateSplit){
        case '01' : {console.log("Januari");break;}
        case '02' : {console.log("Februari");break;}
        case '03' : {console.log("Maret");break;}
        case '04' : {console.log("April");break;}
        case '05' : {console.log("Mei");break;}
        case '06' : {console.log("Juni");break;}
        case '07' : {console.log("Juli");break;}
        default : {console.log("Bulan");break}
    }

    date.sort(function (value1, value2) { return value2 - value1}) 
    console.log(date)//['1989','21','05']

    var hasilJoin = dateJoin.join("-")
    console.log(hasilJoin)// 21-05-1989

    var hasilSlice = inputData.slice(1,2)
    var string = hasilSlice[0].slice(0,14)
    console.log(string) //Roman Alamsyah

}
dataHandling2(inputData)
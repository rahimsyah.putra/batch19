//Soal 1
console.log("----Soal 1----")
var people = [
    ["Bruce", "Banner", "male", 1975], 
    ["Natasha", "Romanoff", "female"],
    ["Tony", "Stark", "male", 1980], 
    ["Pepper", "Pots", "female", 2023]
]

function arrayToObject(people){
    var object = {}
    for (var i=0 ; i<people.length ; i++){
        var dataSementara = people[i]
        for(var j = 0; j <= dataSementara.length; j++){
            var tahunLahir = people[i][3];
            var umurSekarang = new Date().getFullYear()
            if (!tahunLahir){
                age = "Invalid Birth Year"
            } else if (umurSekarang - tahunLahir > 0){
                age = umurSekarang - tahunLahir
            } else if (umurSekarang < tahunLahir ){
                age = "Invalid Birth Year"
            }

            if (j==0){
                object.firstName    = people[i][0]
            } else if (j==1){
                object.lastName     = people[i][1]
            } else if(j==2){
                object.gender       = people[i][2]
            } else if(j==3){
                object.age          = age
            }
        }
    
        var hasil = (i+1) + " . " + object.firstName + " "+object.lastName + " : "

        console.log(hasil)
        console.log(object)
        //console.log("\n")
    }
}
arrayToObject(people)
console.log(" ")

//soal 2

console.log("----Soal 2----")
var memberId = " "
var money =0 ;
function shoppingTime(memberId, money){
    if (!memberId){
        return " Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    } else {
        var newObject = {}
        var moneyChange = money;
        var purchaseList = [];
        var sepatuStacattu = "Sepatu Stacattu";
        var bajuZoro = "Baju Zoro"
        var bajuHNN = "Baju H&N"
        var sweaterUniklooh = "Sweater Uniklooh"
        var casingHP = "Casing Handphone"

        var check = 0;

        for (i=0; moneyChange >= 50000 && check == 0; i++){
            if (moneyChange >= 1500000){
                purchaseList.push(sepatuStacattu)
                moneyChange -= 1500000
            } else if (moneyChange >= 500000){
                purchaseList.push(bajuZoro)
                moneyChange -= 500000
            } else if (moneyChange >= 250000){
                purchaseList.push(bajuHNN)
                moneyChange -= 250000
            } else if (moneyChange >= 175000){
                purchaseList.push(sweaterUniklooh)
                moneyChange -= 175000
            } else if (moneyChange >= 50000){
                for (var j=0; j<=purchaseList.length-1;j++){
                    if(purchaseList[j] == casingHP){
                        check += 1
                    }
                }if (check==0){
                    purchaseList.push(casingHP)
                    moneyChange -= 50000
                } else {
                    purchaseList.push(casingHP)
                    moneyChange -= 50000
                }
            }
        } 

        newObject.memberId = memberId
        newObject.money = money;
        newObject.ListPurchase = purchaseList
        newObject.moneyChange = moneyChange

        return newObject
    }

}

console.log(shoppingTime('1820RzKrnWn08', 2475000));

console.log(shoppingTime('82Ku8Ma742', 170000));

console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log(" ")
//soal 3

console.log("----Soal 3----")
function naikAngkot(listPenumpang){
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var arrOuput = []
    if (listPenumpang.length <= 0){
        return []
    }
    for (var i=0 ; i < listPenumpang.length ; i++){
        var objOutput = {}
        var asal = listPenumpang[i][1]
        var tujuan = listPenumpang[i][2]
        
        var indexAsal;
        var indexTujuan;

        for (var j=0; j<rute.length; j++){
            if(rute[j] == asal){
                indexAsal = j
            } else if (rute[j]== tujuan){
                indexTujuan = j
            }
        }

        var bayar = (indexTujuan - indexAsal) * 2000
        
        objOutput.Penumpang = listPenumpang[i][0]
        objOutput.NaikDari = asal
        objOutput.Tujuan = tujuan
        objOutput.Bayar = bayar

        arrOuput.push(objOutput)
    }
    return arrOuput
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([]))
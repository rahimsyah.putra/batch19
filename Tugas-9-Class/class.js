//Release 1
console.log("--Release 0--")
class Animal{
    constructor(name){
        this._name = name;
        this._legs = 4;
        this._cold_blooded = false;
    }
    get name(){
        return this._name
    }
    get legs(){
        return this._legs
    }
    get cold_blooded(){
        return this._cold_blooded
    }
}

var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

//Release 1
console.log("--Release 1--")

class Ape extends Animal{
    constructor(name, amount){
        super(name)
        this._legs = amount
    }
    yell(){
        console.log("Auoooo")
    }
}

class Frog extends Animal{
    constructor(name){
        super(name)
    }
    jump(){
        console.log("Hop Hop")
    }
}

var sungokong= new Ape ("Kera Sakti", 2)
sungokong.yell()

var kodok = new Frog("Buduk")
kodok.jump()

//Soal 2
console.log("--Soal 2--")

class Clock{
    constructor({template}){
        this.template = template
    }
    render(){
        var date = new Date();
        var hours = date.getHours();
        if (hours < 10){
            hours = "0" + hours
        }

        var minute = date.getMinutes();
        if (minute < 10){
            minute = "0" + minute
        }

        var second = date.getSeconds();
        if (second < 10){
            second = "0" + second
        }

        var output = this.template
        .replace("h", hours)
        .replace("m", minute)
        .replace("s", second)

        console.log(output)
    }
    stop(){
        clearInterval(this.timer)
    }

    start(){
        this.render();
        this.timer = setInterval(()=>this.render(), 1000);
    }
}

var clock = new Clock({template: "h:m:s"});
clock.start();
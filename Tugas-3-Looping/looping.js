//Soal 1 Looping While
//Looping 1
console.log("Soal 1")
var desk1 = "I love coding";
var i = 0;

console.log("LOOPING PERTAMA")
while(i < 20) {
    i += 2;
    console.log( i + " - " +desk1)
}

console.log(" ")

//Looping 2
var desk2 = "I will become a mobile developer";
var k = 22;

console.log("LOOPING KEDUA")
while(k > 2) {
    k -= 2;
    console.log( k + " - " +desk2)
}

console.log(" ")

//Soal 2 Looping menggunakan for
console.log("Soal 2")
var ganjil = "Santai"
var genap = "Berkualitas"
var kelipatanTiga = "I love coding" 

var i = 1;
for (i; i <= 20;i++){
   
    if(i == 3 || i == 9 || i == 15){
        console.log(i + " - " + kelipatanTiga)
    } else if (i %2){
        console.log(i + " - " + ganjil)
    } else {
        console.log(i + " - " + genap)
    }
}

//Soal 3
console.log(" ")
console.log("Soal 3")

for (var i=0 ; i<4;i++){
    var hasil = "#"
    for (var j=0; j<7; j++){
        hasil += "#"
    }
    console.log(hasil)
}

//Soal 4
console.log(" ")
console.log("Soal 4")

for (var i = 0; i < 8; i++) {
    var result = ' ';
    for (var j = 0; j < i; j++) {
         result += '#'
    }    
    console.log(result)
}

//Soal 5
console.log(" ")
console.log("Soal 5")

var result = ""
for (var i=0 ; i<8;i++){
    if (i %2 == 0) {
        result = " "
    } else{
        result = "#"
    } 

    for (var j = 0; j <= 6; j++) {
        
        if(i%2 ==0){
            if (j %2 == 0){
                result += "#"
            } else{
                result += " "
            }
        } else {
            if (j %2 == 0){
                result += " "
            } else{
                result += "#"
            }
        }
    }
    console.log(result)
}
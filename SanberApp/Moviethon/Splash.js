import React, { useEffect } from 'react';
import { StyleSheet, Image, View } from 'react-native';
const Splash = ({navigation}) => {
    useEffect(()=>{
        setTimeout(()=>{
            navigation.replace("Welcome")
        }, 3000)
    }, [])

    return (
        <View style={{justifyContent:'center', alignItems:'center', flex:1}}>
            <Image source={require('./assets/images/moviethon.png')}/>
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({})

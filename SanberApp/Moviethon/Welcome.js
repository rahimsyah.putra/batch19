import React from 'react'
import { StyleSheet, Image, TouchableOpacity, Text, View } from 'react-native'

const Welcome = ({navigation}) => {
    return (
        <View style={styles.container}>
            <View style={styles.posisi}>
                <Image source={require('./assets/images/moviethon.png')} 
                style={{height:400, width:300}}/>
            </View>
            <View style={styles.kotak}>
                <Text style={styles.textSignUp}>Welcome</Text>
                <View style={styles.kotakLogin}>
                    <TouchableOpacity style={styles.btnReg}
                        onPress={()=>navigation.navigate('Register')}>
                        <Text style={styles.textbtn}>Register</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnLog} 
                        onPress={()=>navigation.navigate('Login')}>
                        <Text style={styles.textbtn}>Login</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default Welcome

const styles = StyleSheet.create({
    container: {
        paddingTop: 5,
        backgroundColor:'#D2CBCB'
    },
    posisi:{
        marginTop:30,
        marginHorizontal: 50
    },
    textSignUp: {
        fontSize: 24,
        marginTop: 10,
        color:'#ffffff',
        marginVertical: 20,
        marginHorizontal:20
    },
    text:{
        color:'#ffffff',
        marginHorizontal:30
    },
    input:{
        height:40,
        borderColor:"#003366",
        padding: 10,
        borderWidth:1
    },
    textbtn: {
        fontSize: 24,
        color: '#000000'
    },
    btnReg: { 
        alignItems: 'center',
        backgroundColor: '#ffffff',
        padding:10,
        borderRadius:20,
        marginHorizontal: 15,
        marginBottom:20,
        width:370
    },
    btnLog: { 
        alignItems: 'center',
        backgroundColor: '#B9CDFF',
        padding:10,
        borderRadius:20,
        marginHorizontal: 15,
        marginBottom:70,
        width:370
    },
    kotak:{
        borderColor:'blue',
        borderRadius:30,
        borderBottomColor:'#000',
        padding:5,
        backgroundColor:'#4689AF',
        marginTop:130,
        width:412
    },
})

import React, {Component} from 'react'
import { StyleSheet, Text, View } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import Home from './Home';
import Login from './Login';
import Profile from './Profile';
import Register from './Register';
import Splash from './Splash';
import Welcome from './Welcome';
import Detail from './Detail';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const index = () => {
    return (
        <NavigationContainer >
            <Stack.Navigator initialRouteName={Splash}>
                <Stack.Screen name="Splash" component={Splash}
                options={{
                    headerShown:false
                    }}/>
                <Stack.Screen name="Login" component={Login} 
                options={{
                    headerShown:false
                    }}/>
                <Stack.Screen name="Profile" component={Profile}
                options={{
                    headerShown:false
                    }}/>
                <Stack.Screen name="Register" component={Register}
                options={{
                    headerShown:false
                    }}/>
                <Stack.Screen name="Welcome" component={Welcome}
                options={{
                    headerShown:false
                    }}/>
                <Stack.Screen name="MainApp" component={MainApp}
                options={{
                    headerShown:false
                    }}/>
                <Stack.Screen name="Detail" component={Detail}
                options={{
                    headerShown:false
                    }}/>
            </Stack.Navigator>
      </NavigationContainer>
    )
}
const MainApp =() =>{
    return(
        <Tab.Navigator
            tabBarOptions={{
                labelStyle:{
                    fontSize:15
                }
            }}>
        <Tab.Screen 
            name="Home"
            component={Home} 
            options={{
                tabBarIcon: ()=> <Icon name="home" size={25}/>  
            }}/>
        <Tab.Screen 
            name="Profile" 
            component={Profile} 
            options={{
                tabBarIcon: ()=> <Icon name="account-circle" size={25}/>  
            }}/>
      </Tab.Navigator>
    )
}

export default index

const styles = StyleSheet.create({})

import React from 'react'
import { 
    Image,
    StyleSheet,
    Text,
    TextInput,
    Button,
    TouchableOpacity,
    View } from 'react-native'

const Register = ({navigation}) => {
    return (
        <View style={styles.container}>
            <Image source={require('./assets/images/moviethon.png')} style={{height:400, width:400}}/>
            <View style={styles.kotak}>
                <Text style={styles.textSignUp}>Sign Up</Text>
                <View >
                    <Text style={styles.text}>Username</Text>
                    <TextInput style={styles.input} style={styles.formInput} />
                </View>
                <View >
                    <Text style={styles.text}>Email</Text>
                    <TextInput style={styles.input} style={styles.formInput} />
                </View>
                <View >
                    <Text style={styles.text}>Password</Text>
                    <TextInput style={styles.input} style={styles.formInput} secureTextEntry={true}/>
                </View>
                <View style={styles.kotakLogin}>
                    <TouchableOpacity 
                        style={styles.btnReg}
                        onPress={()=>navigation.navigate('Login')}>
                        <Text style={styles.textbtn}>SIGN UP</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default Register

const styles = StyleSheet.create({
    container: {
        flex:1,
        paddingTop: 5,
        backgroundColor:'#D2CBCB'
    },
    formInput: {
        marginHorizontal: 30,
        marginVertical:5,
        alignContent:'center',
        width:350,
        height:40,
        backgroundColor:'#ffffff',
        borderRadius:10,
        borderWidth:1,
        includeFontPadding:true,
        padding:10
    },
    textSignUp: {
        fontSize: 24,
        marginTop: 10,
        color:'#ffffff',
        marginVertical: 20,
        marginHorizontal:20,
        marginBottom:20
    },
    text:{
        color:'#ffffff',
        marginHorizontal:30
    },
    input:{
        height:20,
        borderColor:"#003366",
        padding: 20,
        borderWidth:1,
        fontSize:20
    },
    textbtn: {
        fontSize: 24,
        fontWeight:'bold',
        color: '#ffffff'
    },
    btnReg: { 
        alignItems: 'center',
        padding:10,
        marginTop:10,
        marginHorizontal: 255,
        marginBottom:40,
        width:140
    },
    kotak:{
        flex:1,
        borderColor:'blue',
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        borderBottomColor:'#000',
        padding:5,
        backgroundColor:'#4689AF',
        marginTop:5,
        width:412
    },
});
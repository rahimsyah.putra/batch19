import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, FlatList, Image, ImageBackground } from 'react-native'
import { ScrollView, TouchableOpacity } from 'react-native'

import Var from './Var'
import Icon from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';


const Home = ({ navigation }) => {
    const [genres, setGenres] = useState([]);
    const [topRated, setTopRated] = useState([]);
    const [popular, setPopular] = useState([]);
    const [upcoming, setUpcoming] = useState([]);

    useEffect(() => {
        async function getGenres() {
            try {
                let response = await fetch(
                    Var.host + "genre/movie/list?api_key=" + Var.api_key_tmdb
                );
                let json = await response.json();
                setGenres(json.genres)
            } catch (error) {
                console.error(error);
            }
        }
        getGenres();

        async function getTopRated() {
            try {
                let response = await fetch(
                    Var.host + "movie/top_rated?api_key=" + Var.api_key_tmdb
                );
                let json = await response.json();
                setTopRated(json.results)
            } catch (error) {
                console.error(error);
            }
        }
        getTopRated();

        async function getPopular() {
            try {
                let response = await fetch(
                    Var.host + "movie/popular?api_key=" + Var.api_key_tmdb
                );
                let json = await response.json();
                setPopular(json.results)
            } catch (error) {
                console.error(error);
            }
        }
        getPopular();

        async function getUpcoming() {
            try {
                let response = await fetch(
                    Var.host + "movie/upcoming?api_key=" + Var.api_key_tmdb
                );
                let json = await response.json();
                setUpcoming(json.results)
            } catch (error) {
                console.error(error);
            }
        }
        getUpcoming();
    }, []);

    return (
        <View style={styles.container}>
            <ScrollView>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.5, margin: 10 }}>
                        <Text style={styles.textJudul}>MOVIETHON</Text>
                    </View>
                    <TouchableOpacity style={styles.navBar}>
                        <Icon name="search" color={'white'} size={30} />
                    </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <FlatList
                        style={{ maxHeight: 50 }}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        data={genres}
                        keyExtractor={item => item.id}
                        renderItem={({ item }) => <RenderItem item={item} />}
                    />
                </View>

                <View>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 0.5 }}>
                            <Text style={styles.textTema}>TOP RATED</Text>
                        </View>
                        <View style={styles.posisiTema} >
                            <AntDesign name="right" size={25} color="#F09631" />
                        </View>
                    </View>

                    <FlatList
                        contentContainerStyle={{ paddingHorizontal: 10 }}
                        style={{ maxHeight: 250 }}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        data={topRated}
                        keyExtractor={item => item.id}
                        renderItem={({ item }) => <RenderItemTopRated item={item} />}
                    />
                </View>

                <View>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 0.5 }}>
                            <Text style={styles.textTema}>POPULAR</Text>
                        </View>
                        <View style={styles.posisiTema} >
                            <AntDesign name="right" size={25} color="#F09631" />
                        </View>
                    </View>
                    <FlatList
                        contentContainerStyle={{ paddingHorizontal: 10 }}
                        style={{ maxHeight: 250 }}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        data={popular}
                        keyExtractor={item => item.id}
                        renderItem={({ item, index }) => <RenderItemPopular item={item}
                        />}
                    />
                </View>
                <View>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 0.5 }}>
                            <Text style={styles.textTema}>UPCOMING</Text>
                        </View>
                        <View style={styles.posisiTema} >
                            <AntDesign name="right" size={25} color="#F09631" />
                        </View>
                    </View>
                    <FlatList
                        contentContainerStyle={{ paddingHorizontal: 10 }}
                        style={{ maxHeight: 250 }}
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        data={upcoming}
                        keyExtractor={item => item.id}
                        renderItem={({ item }) => <RenderItemupcoming item={item}
                            onPress={() => { navigation.navigate('Detail') }} />}
                    />
                </View>
            </ScrollView>
        </View>
    )

    function press(item) {
        navigation.navigate('Detail', {
            item: item
        })
    }

    function RenderItem({ item }) {
        return (
            <TouchableOpacity>
                <View style={styles.barGenre}>
                    <Text style={{ color: 'white' }}>{item.name}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    function RenderItemTopRated({ item }) {
        let year = item.release_date.split('-');
        return (
            <TouchableOpacity onPress={() => press(item)}>
                <ImageBackground
                    source={{ uri: 'https://image.tmdb.org/t/p/w500/' + item.poster_path }}
                    style={styles.poster} resizeMode={"cover"}>
                    <Text style={styles.textDeskripsi}
                        numberOfLines={2}
                        ellipsizeMode={"tail"}
                    >{item.title}</Text>

                    <Text style={styles.rating}>
                        {item.vote_average}
                    </Text>
                    <Text style={styles.releaseYear}>
                        {year[0]}
                    </Text>

                </ImageBackground>
            </TouchableOpacity>
        )
    }

    function RenderItemPopular({ item }) {
        let year = item.release_date.split('-');
        return (
            <TouchableOpacity onPress={() => press(item)}>
                <ImageBackground source={{ uri: 'https://image.tmdb.org/t/p/w500/' + item.poster_path }}
                    style={styles.poster}
                    resizeMode={"cover"}>
                    <Text style={styles.textDeskripsi}
                        numberOfLines={2}
                        ellipsizeMode={"tail"}
                    >{item.title}</Text>
                    <Text style={styles.rating}>
                        {item.vote_average}
                    </Text>
                    <Text style={styles.releaseYear}>
                        {year[0]}
                    </Text>
                </ImageBackground>
            </TouchableOpacity>
        )
    }

    function RenderItemupcoming({ item }) {
        let year = item.release_date.split('-');
        return (
            <TouchableOpacity onPress={() => press(item)}>
                <ImageBackground source={{ uri: 'https://image.tmdb.org/t/p/w500/' + item.poster_path }}
                    style={styles.poster}
                    resizeMode={"cover"}>
                    <Text style={styles.textDeskripsi}
                        numberOfLines={2}
                        ellipsizeMode={"tail"}
                    >{item.title}</Text>
                    <Text style={styles.rating}>
                        {item.vote_average}
                    </Text>
                    <Text style={styles.releaseYear}>
                        {year[0]}
                    </Text>
                </ImageBackground>
            </TouchableOpacity >
        )
    }
}

export default Home

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#D2CBCB',
        marginTop: 40
    },
    poster: {
        width: 150,
        height: 220,
        marginRight: 10,
        justifyContent: 'flex-end',
    },
    kotak: {
        flex: 1,
        borderColor: 'blue',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        borderBottomColor: '#000',
        padding: 5,
        backgroundColor: '#4689AF',
        marginTop: 70,
        marginBottom: 70,
        width: 412
    },
    textJudul: {
        fontSize: 20,
        color: '#003366',
        fontWeight: 'bold'
    },
    navBar: {
        flex: 0.5,
        alignItems: 'flex-end',
        justifyContent: 'center',
        margin: 10
    },
    barGenre: {
        backgroundColor: '#F09631',
        alignSelf: 'flex-start',
        margin: 5,
        padding: 10,
        borderRadius: 5
    },
    posisiTema: {
        flex: 0.5,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    textTema: {
        color: '#003366',
        fontSize: 20,
        margin: 10,
        fontWeight: 'bold'
    },
    textDeskripsi: {
        color: 'white',
        padding: 5,
        backgroundColor: 'rgba(0,0,0,0.4)'
    },
    rating: {
        backgroundColor: '#F09631',
        alignSelf: 'flex-start',
        padding: 5,
        position: 'absolute',
        top: 5,
        right: 5
    },
    releaseYear: {
        color: '#F09631',
        backgroundColor: 'rgba(0,0,0,0.4)',
        alignSelf: 'flex-start',
        padding: 5,
        position: 'absolute',
        top: 5,
        left: 5
    },
})

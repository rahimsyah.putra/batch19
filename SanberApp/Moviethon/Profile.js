import React from 'react'
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image
} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'

const Profile = ({navigation}) => {
    return (
        <ScrollView>
            <View style={styles.container}>
                <Text style={styles.textJudul}>About Me</Text>
                <View style={styles.formProfil}>
                    <View>
                        <Image source={require('./assets/images/man.png')}
                            style={styles.account} />
                    </View>
                    <Text style={styles.textNama}>M. Rahimsyah Putra</Text>
                    <Text style={styles.textSkill}>Mobile Developer</Text>
                </View>
                <View style={styles.kotak}>
                    <Text style={styles.textDalam}>Portofolio</Text>
                    <View style={styles.kotakDalam}>
                        <View style={styles.icon}>
                            <Image source={require('./assets/images/gitlab.png')} style={styles.photo} />
                            <Text style={styles.username}>@Rahimsyah.putra</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.kotak}>
                    <Text style={styles.textDalam}>Find me on</Text>
                    <View style={styles.kotakDalam}>
                        <View style={styles.kotakDalamHub}>
                            <View style={styles.posisi}>
                                <Image source={require('./assets/images/instagram.png')} style={styles.photo} />
                                <Text style={styles.username}>@rahimsyahputra</Text>
                            </View>
                        </View>
                    </View>
                    
                </View>
                <TouchableOpacity
                        onPress={() => navigation.navigate('Login')}
                        style={styles.btnLogout}>
                        <Text style={styles.textBtn}>LOGOUT</Text>
                    </TouchableOpacity>
            </View>
        </ScrollView>
    )
}

export default Profile

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#D2CBCB',
        marginTop: 40,
        paddingHorizontal: 30
    },
    kotak: {
        borderColor: 'blue',
        borderRadius: 10,
        borderBottomColor: '#000',
        padding: 5,
        backgroundColor: '#46DAAD',
        marginBottom: 10
    },
    kotakDalam: {
        borderTopWidth: 2,
        borderTopColor: '#003366',
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginVertical: 5,
        alignContent: 'center',
    },
    kotakDalamHub: {
        borderTopColor: '#003366',
        flexDirection: 'column',
        justifyContent: 'space-around',
        marginVertical: 5,
        alignContent: 'center',
    },
    textJudul: {
        fontSize: 24,
        marginTop: 30,
        textAlign: 'center',
        color: '#003366',
        marginVertical: 20,
        fontWeight: 'bold'
    },
    textDalam: {
        fontSize: 14,
        color: '#003366'
    },
    textNama: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#003366',
        textAlign: "center"
    },
    textSkill: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#3EC6FF',
        textAlign: "center",
        marginBottom: 5
    },
    photo: {
        width: 40,
        height: 40,
        alignSelf: 'center',
        padding: 5,
        marginTop: 5
    },
    username: {
        fontSize: 18,
        color: '#003366',
        marginLeft: 10,
        marginTop: 10
    },
    icon: {
        alignSelf: 'center',
        padding: 10,
    },
    posisi: {
        flexDirection: "column",
        marginVertical: 5,
        justifyContent: 'center'
    },
    account: {
        flex: 1,
        height: 200,
        width: 200,
        alignSelf: 'center',
        marginBottom: 15
    },
    btnLogout: {
        width: 100,
        height: 30,
        backgroundColor: '#4689AF',
        alignSelf: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        marginTop:20,
        marginBottom:30
    },
    textBtn: {
        color:'white',
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 5
    },
});
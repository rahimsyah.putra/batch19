import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View,  Image,  TouchableOpacity, ScrollView, StatusBar } from 'react-native'


const Detail = ({ navigation, route }) => {
    const { item } = route.params;
    return (
        <ScrollView>
            <View style={styles.container}>
                <View style={styles.kotak}>
                    <Text style={styles.textJudul}>{item.title}</Text>
                    <Image source={{ uri: 'https://image.tmdb.org/t/p/w500/' + item.poster_path }}
                        style={styles.poster} />
                    <View style={{ flexDirection: 'row', margin: 5 }}>
                        <View style={styles.textYear}>
                            <Text style={styles.textDeskripsi}>Release : {item.release_date}</Text>
                        </View>
                        <View style={styles.textRate}>
                            <Text style={styles.textDeskripsi}>Rating : {item.vote_average}</Text>
                        </View>
                    </View>


                </View>
                <View style={styles.kotakDesk}>
                    <Text style={styles.textDeskripsi}>{item.overview}</Text>
                </View>
                <TouchableOpacity
                    onPress={() => navigation.navigate('Home')}
                    style={styles.btnBack}>
                    <Text style={styles.textBtn}>BACK</Text>
                </TouchableOpacity>
            </View>
        </ScrollView>
    )
}

export default Detail

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#D2CBCB',
        marginTop: 30,
    },
    poster: {
        width: 200,
        height: 300,
        alignSelf: 'center',
        marginBottom: 10,
        borderRadius: 20,
        shadowRadius: 30,
        shadowOpacity: 10,
    },
    kotak: {
        marginTop: 40,
        borderColor: 'blue',
        borderRadius: 10,
        borderBottomColor: '#000',
        padding: 5,
        backgroundColor: '#4689AF',
        marginBottom: 19,
    },
    kotakDesk: {
        marginTop: 10,
        borderColor: 'blue',
        borderRadius: 10,
        borderBottomColor: '#000',
        padding: 5,
        backgroundColor: '#4689AF',
        marginBottom: 19,
    },
    btnBack: {
        width: 100,
        height: 30,
        backgroundColor: '#4689AF',
        alignSelf: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        marginBottom: 40
    },
    textJudul: {
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold'
    },
    textDeskripsi: {
        color: 'white',
        padding: 10,
        textAlign: 'justify',
        fontSize: 18
    },
    textBtn: {
        fontSize: 20,
        color:'white',
        fontWeight: 'bold',
        textAlign: 'center',
        padding: 5
    },
    textYear: {
        backgroundColor: 'rgba(0,0,0,0.5)',
        borderRadius: 15,
    },
    textRate: {
        justifyContent: 'center',
        left: 75,
        backgroundColor: 'rgba(0,0,0,0.5)',
        borderRadius: 15
    },
})
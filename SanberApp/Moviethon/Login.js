import React from 'react'
import {
    Image,
    StyleSheet,
    Text,
    TextInput,
    Button,
    TouchableOpacity,
    View } from 'react-native'

const Login = ({navigation}) => {
    return (
        <View style={styles.container}>
            <Image source={require('./assets/images/moviethon.png')} style={{height:400, width:400}}/>
            <View style={styles.kotak}>
                <Text style={styles.textSignIn}>Sign In</Text>
                <View >
                    <Text style={styles.text}>Username / Email</Text>
                    <TextInput style={styles.input} style={styles.formInput} />
                </View>
                <View >
                    <Text style={styles.text}>Password</Text>
                    <TextInput style={styles.input} style={styles.formInput} secureTextEntry={true}/>
                </View>
                <View style={styles.kotakLogin}>
                    <TouchableOpacity 
                        style={styles.btnLog}
                        onPress={()=>navigation.navigate('MainApp')} >
                        <Text style={styles.textbtn}>SIGN IN</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default Login

const styles = StyleSheet.create({
    container: {
        flex:1,
        paddingTop: 5,
        backgroundColor:'#D2CBCB'
    },
    formInput: {
        marginHorizontal: 30,
        marginVertical:5,
        alignContent:'center',
        width:340,
        height:40,
        backgroundColor:'#ffffff',
        borderRadius:10,
        borderWidth:1,
        includeFontPadding:true,
        padding:10
    },
    textSignIn: {
        fontSize: 24,
        marginTop: 10,
        color:'#ffffff',
        marginVertical: 20,
        marginHorizontal:20
    },
    text:{
        color:'#ffffff',
        marginHorizontal:30
    },
    input:{
        height:40,
        borderColor:"#003366",
        padding: 10,
        borderWidth:1
    },
    textbtn: {
        fontWeight:'bold',
        fontSize: 24,
        color: '#ffffff'
    },
    btnLog: { 
        alignItems: 'center',
        padding:10,
        marginTop:20,
        marginHorizontal: 255,
        marginBottom:10,
        width:140
    },
    kotak:{
        flex:1,
        borderColor:'blue',
        borderTopLeftRadius:30,
        borderTopRightRadius:30,
        borderBottomColor:'#000',
        padding:5,
        backgroundColor:'#4689AF',
        marginTop:70,
        width:412
    },
});

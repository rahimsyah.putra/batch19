import React from 'react'
import {
    StyleSheet,
    View,
    Text,
    Image,
    TextInput,
    Button,
    ScrollView,
} from 'react-native'

const AboutScreen=()=> {
    return(
        <ScrollView>
            <View style={styles.container}>
                <Text style={styles.textJudul}>Tentang Saya</Text>
                <View style={styles.formProfil}>
                    <Image source={require('./assets/account-circle.png')} style={styles.account}/>
                    <Text style={styles.textNama}>M. Rahimsyah Putra</Text>
                    <Text style={styles.textSkill}>React Native Developer</Text>
                </View>
                <View style={styles.kotak}>
                    <Text style={styles.textDalam}>Portofolio</Text>
                    <View style={styles.kotakDalam}>
                        <View style={styles.icon}>
                            <Image source={require('./assets/gitlab.png')} style={styles.photo}/>
                            <Text style={styles.username}>@Rahimsyah.putra</Text>
                        </View>
                        <View style={styles.icon}>
                            <Image source={require('./assets/github.png')} style={styles.photo}/>
                            <Text style={styles.username}>@Rahimsyah.putra</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.kotak}>
                    <Text style={styles.textDalam}>Hubungi Saya</Text>
                    <View style={styles.kotakDalam}>
                        <View style={styles.kotakDalamHub}>
                            <View >
                                <View style={styles.posisi}>
                                    <Image source={require('./assets/facebook.png')} style={styles.photo}/>
                                    <Text style={styles.username}>@Rahimsyah.putra</Text>
                                </View>
                                <View style={styles.posisi}>
                                    <Image source={require('./assets/instagram.png')} style={styles.photo}/>
                                    <Text style={styles.username}>@Rahimsyah.putra</Text>
                                </View>
                                <View style={styles.posisi}>
                                    <Image source={require('./assets/twiter.png')} style={styles.photo}/>
                                    <Text style={styles.username}>@Rahimsyah.putra</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                    
                </View>
            </View>
        </ScrollView>
    );
}

export default AboutScreen

const styles = StyleSheet.create({
    container: {
        marginTop:40,
        paddingHorizontal: 30
    },
    kotak:{
        borderColor:'blue',
        borderRadius:10,
        borderBottomColor:'#000',
        padding:5,
        backgroundColor:'#EFEFEF',
        marginBottom:9
    },
    kotakDalam: {
        borderTopWidth: 2,
        borderTopColor: '#003366',
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginVertical:5,
        alignContent:'center',
    },
    kotakDalamHub: {
        borderTopColor: '#003366',
        flexDirection: 'column',
        justifyContent: 'space-around',
        marginVertical:5,
        alignContent:'center',
    },
    textJudul: {
        fontSize: 24,
        marginTop: 63,
        textAlign:'center',
        color:'#003366',
        marginVertical: 20,
        fontWeight:'bold'
    },
    textDalam:{
        fontSize: 14,
        color:'#003366'
    },
    textNama :{
        fontSize:16,
        fontWeight:'bold',
        color:'#003366',
        textAlign:"center"
    },
    textSkill:{
        fontSize:16,
        fontWeight:'bold',
        color:'#3EC6FF',
        textAlign:"center",
        marginBottom:5
    },
    photo:{
        width:30,
        height:30,
        alignItems:'center',
        padding:5,
        marginTop:5
    },
    username: {
        fontSize: 12,
        color: '#003366',
        marginLeft: 10,
        marginTop:10
    },
    icon:{
        alignContent:'center',
        padding: 10,
    },
    posisi:{
        flexDirection: "row",
        marginVertical:5,
        justifyContent:'center'
    },
    account:{
        height:200,
        width:200,
        alignItems:'center',
        marginLeft:75,
        marginBottom:5
    }
});
import React from 'react'
import {
    StyleSheet,
    View,
    Text,
    Image,
    TextInput,
    Platform,
    TouchableOpacity, 
    KeyboardAvoidingView,
    ScrollView,
} from 'react-native'

//import Icon from 'react-native-vector-icons/MaterialIcons';

//export default class LoginScreen extends React.Component {
const LoginScreen=()=> {
    return(
        <KeyboardAvoidingView
        behavior ={Platform.OS == "ios" ? "padding" : "height"}
        style = {styles.container}
        >
        <ScrollView>
            <View>
                <Image source={require('./assets/logoSanber.png')}/>
                <Text style={styles.textLogin}>Login</Text>
                <View style={styles.formInput}>
                    <Text style={styles.text}>Username / Email</Text>
                    <TextInput style={styles.input} />
                </View>
                <View style={styles.formInput}>
                    <Text style={styles.text}>Password</Text>
                    <TextInput style={styles.input} secureTextEntry={true}/>
                </View>
                <View style={styles.kotakLogin}>
                    <TouchableOpacity style={styles.btnLogin}>
                        <Text style={styles.textbtn}>Masuk</Text>
                    </TouchableOpacity>
                    <Text style={styles.textOr}>Atau</Text>
                    <TouchableOpacity style={styles.btnReg}>
                        <Text style={styles.textbtn}>Daftar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
        </KeyboardAvoidingView>
    );
}

export default LoginScreen

const styles = StyleSheet.create({
    container: {
        flex:1,
        paddingTop: 16,
        paddingHorizontal: 30
    },
    formInput: {
        marginHorizontal: 30,
        marginVertical:5,
        alignContent:'center',
        width:294
    },
    textLogin: {
        fontSize: 24,
        marginTop: 63,
        textAlign:'center',
        color:'#003366',
        marginVertical: 20
    },
    text:{
        color:'#003366'
    },
    input:{
        height:40,
        borderColor:"#003366",
        padding: 10,
        borderWidth:1
    },
    textOr: {
        fontSize: 22,
        color: '#003366',
        textAlign: 'center',
        marginBottom: 10
    },
    btnLogin: {
        alignItems: 'center',
        backgroundColor: '#3EC6FF',
        padding:10,
        borderRadius:20,
        marginTop:40,
        marginHorizontal: 105,
        marginBottom:10,
        width:140
    },
    textbtn: {
        fontSize: 18,
        color: '#ffffff'
    },
    btnReg: { 
        alignItems: 'center',
        backgroundColor: '#003366',
        padding:10,
        borderRadius:20,
        marginHorizontal: 105,
        marginBottom:10,
        width:140
    },
});